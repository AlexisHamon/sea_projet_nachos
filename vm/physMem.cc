//-----------------------------------------------------------------
/*! \file mem.cc
  \brief Routines for the physical page management

 * -----------------------------------------------------
 * This file is part of the Nachos-RiscV distribution
 * Copyright (c) 2022 University of Rennes 1.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details
 * (see see <http://www.gnu.org/licenses/>).
 * -----------------------------------------------------
 */

#include "vm/physMem.h"
#include "kernel/msgerror.h"
#include "kernel/system.h"
#include "machine/interrupt.h"
#include "machine/translationtable.h"
#include "utility/config.h"
#include "utility/utility.h"
#include <cstddef>
#include <cstdint>
#include <random>
#include <unistd.h>

//-----------------------------------------------------------------
// PhysicalMemManager::PhysicalMemManager
//
/*! Constructor. It simply clears all the page flags and inserts them in the
// free_page_list to indicate that the physical pages are free
*/
//-----------------------------------------------------------------
PhysicalMemManager::PhysicalMemManager() {

  uint64_t i;
  tpr = new struct tpr_c[g_cfg->NumPhysPages];

  for (i = 0; i < g_cfg->NumPhysPages; i++) {
    tpr[i].free = true;
    tpr[i].locked = false;
    tpr[i].owner = NULL;
    free_page_list.Append((void *)i);
  }
  i_clock = 0;
}

PhysicalMemManager::~PhysicalMemManager() {
  // Empty free page list
  while (!free_page_list.IsEmpty())
    free_page_list.Remove();

  // Delete physical page table
  delete[] tpr;
}

//-----------------------------------------------------------------
// PhysicalMemManager::RemovePhysicalToVitualMapping
//
/*! This method releases an unused physical page by clearing the
//  corresponding bit in the page_flags bitmap structure, and adding
//  it in the free_page_list.
//
//  \param num_page is the number of the real page to free
*/
//-----------------------------------------------------------------
void PhysicalMemManager::RemovePhysicalToVirtualMapping(uint64_t num_page) {

  // Check that the page is not already free
  ASSERT(!tpr[num_page].free);

  // Update the physical page table entry
  tpr[num_page].free = true;
  tpr[num_page].locked = false;
  if (tpr[num_page].owner->translationTable != NULL)
    tpr[num_page].owner->translationTable->clearBitValid(
        tpr[num_page].virtualPage);

  // Insert the page in the free list
  free_page_list.Prepend((void *)num_page);
}

//-----------------------------------------------------------------
// PhysicalMemManager::UnlockPage
//
/*! This method unlocks the page numPage, after
//  checking the page is in the locked state. Used
//  by the page fault manager to unlock at the
//  end of a page fault (the page cannot be evicted until
//  the page fault handler terminates).
//
//  \param num_page is the number of the real page to unlock
*/
//-----------------------------------------------------------------
void PhysicalMemManager::UnlockPage(uint64_t num_page) {
  ASSERT(num_page < g_cfg->NumPhysPages);
  ASSERT(tpr[num_page].locked == true);
  ASSERT(tpr[num_page].free == false);
  tpr[num_page].locked = false;
}

//-----------------------------------------------------------------
// PhysicalMemManager::ChangeOwner
//
/*! Change the owner of a page
//
//  \param owner is a pointer on new owner (Thread *)
//  \param numPage is the concerned page
*/
//-----------------------------------------------------------------
void PhysicalMemManager::ChangeOwner(uint64_t numPage, Thread *owner) {
  // Update statistics
  g_current_thread->GetProcessOwner()->stat->incrMemoryAccess();
  // Change the page owner
  tpr[numPage].owner = owner->GetProcessOwner()->addrspace;
}

//-----------------------------------------------------------------
// PhysicalMemManager::AddPhysicalToVirtualMapping
//
/*! This method returns a new physical page number. If there is no
//  page available, it evicts one page (page replacement algorithm).
//
//  NB: this method locks the newly allocated physical page such that
//      it is not stolen during the page fault resolution. Don't forget
//      to unlock it
//
//  \param owner address space (for backlink)
//  \param virtualPage is the number of virtualPage to link with physical page
//  \return A new physical page number.
*/
//-----------------------------------------------------------------
int PhysicalMemManager::AddPhysicalToVirtualMapping(AddrSpace *owner,
                                                    uint64_t virtualPage) {
  int pp = g_physical_mem_manager->EvictPage();

  if (pp == INVALID_PAGE) {
    printf("Not enough free space to load a page.\n");

    g_machine->interrupt->Halt(ERROR);
  }

  /* Initialise la page virtuelle dans la tpr */
  g_physical_mem_manager->tpr[pp].virtualPage = virtualPage;
  g_physical_mem_manager->tpr[pp].owner = owner;
  g_physical_mem_manager->tpr[pp].locked = true;

  /* Initialise la page dans la table du processus */
  owner->translationTable->setPhysicalPage(virtualPage, pp);

  return pp;
}

//-----------------------------------------------------------------
// PhysicalMemManager::FindFreePage
//
/*! This method returns a new physical page number, if it finds one
//  free. If not, return INVALID_PAGE. Does not run the clock algorithm.
//
//  \return A new free physical page number.
*/
//-----------------------------------------------------------------
int PhysicalMemManager::FindFreePage() {
  uint64_t page;

  // Check that the free list is not empty
  if (free_page_list.IsEmpty()) {
    return INVALID_PAGE;
  }

  // Update statistics
  ASSERT(g_current_thread->GetProcessOwner() != NULL);
  g_current_thread->GetProcessOwner()->stat->incrMemoryAccess();

  // Get a page from the free list
  page = (int64_t)free_page_list.Remove();

  // Check that the page is really free
  ASSERT(tpr[page].free);

  // Update the physical page table
  tpr[page].free = false;

  return page;
}

//-----------------------------------------------------------------
// PhysicalMemManager::EvictPage
//
/*! This method implements page replacement, using the well-known
//  clock algorithm.
//
//  \return A new free physical page number.
*/
//-----------------------------------------------------------------
#ifndef ETUDIANTS_TP
int PhysicalMemManager::EvictPage() {
  printf("**** Warning: page replacement algorithm is not implemented yet\n");

  exit(ERROR);

  return (0);
}
#endif

#ifdef ETUDIANTS_TP


void PhysicalMemManager::PutToSwap(uint64_t k) {

  uint64_t virtualPage = tpr[k].virtualPage;

  TranslationTable* translationTable = tpr[k].owner->translationTable;

  int8_t* mainMemory_pp = &(g_machine->mainMemory[k * g_cfg->PageSize]);

  if (translationTable->getBitM(virtualPage)) {
    translationTable->setAddrDisk(virtualPage, g_swap_manager->PutPageSwap(translationTable->getAddrDisk(virtualPage), (char*)mainMemory_pp));
    translationTable->setBitSwap(virtualPage);
  }
    
  RemovePhysicalToVirtualMapping(k);
  translationTable->clearBitIo(virtualPage);
}

uint64_t PhysicalMemManager::get_free_page() {
  /* ASSERT(g_machine->interrupt->GetStatus() == INTERRUPTS_OFF); */

  auto page = FindFreePage();
  while (page == INVALID_PAGE) {
    uint64_t k_loop = (i_clock + 1) % g_cfg->NumPhysPages;
    auto old_state = g_machine->interrupt->SetStatus(IntStatus::INTERRUPTS_OFF);
    while (tpr[k_loop].locked) {
      k_loop = (k_loop + 1) % g_cfg->NumPhysPages;
      if (k_loop == i_clock) {
        DEBUG('w', (char*) "Toutes les pages sont vérouillées...\n");
        g_current_thread->Yield();
      }
    }
    tpr[k_loop].locked = true;
    uint64_t virtualPage = tpr[k_loop].virtualPage;
    TranslationTable* translationTable = tpr[k_loop].owner->translationTable;

    translationTable->clearBitValid(virtualPage);
    translationTable->setBitIo(virtualPage);
    g_machine->interrupt->SetStatus(old_state);
    PutToSwap(k_loop);
    i_clock = k_loop;
    page = FindFreePage();
  }
  return page;
}




int PhysicalMemManager::EvictPage() {
  return get_free_page();
}

#endif
//-----------------------------------------------------------------
// PhysicalMemManager::Print
//
/*! print the current status of the table of physical pages
//
//  \param rpage number of real page
*/
//-----------------------------------------------------------------

void PhysicalMemManager::Print(void) {
  uint64_t i;

  printf("Contents of TPR (%" PRIu64 " pages)\n", g_cfg->NumPhysPages);
  for (i = 0; i < g_cfg->NumPhysPages; i++) {
    printf("Page %" PRIu64 " free=%d locked=%d virtpage=%" PRIu64
           " owner=%lx U=%d M=%d\n",
           i, tpr[i].free, tpr[i].locked, tpr[i].virtualPage,
           (long int)tpr[i].owner,
           (tpr[i].owner != NULL)
               ? tpr[i].owner->translationTable->getBitU(tpr[i].virtualPage)
               : 0,
           (tpr[i].owner != NULL)
               ? tpr[i].owner->translationTable->getBitM(tpr[i].virtualPage)
               : 0);
  }
}
