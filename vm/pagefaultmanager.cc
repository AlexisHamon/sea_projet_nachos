/*! \file pagefaultmanager.cc
Routines for the page fault managerPage Fault Manager
 * -----------------------------------------------------
 * This file is part of the Nachos-RiscV distribution
 * Copyright (c) 2022 University of Rennes 1.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details
 * (see see <http://www.gnu.org/licenses/>).
 * -----------------------------------------------------
*/

#include "vm/pagefaultmanager.h"
#include "kernel/msgerror.h"
#include "kernel/thread.h"
#include "vm/physMem.h"
#include "vm/swapManager.h"

PageFaultManager::PageFaultManager() {}

// PageFaultManager::~PageFaultManager()
/*! Nothing for now
 */
PageFaultManager::~PageFaultManager() {}

// ExceptionType PageFault(uint64_t virtualPage)
/*!
//	This method is called by the Memory Management Unit when there is a
//      page fault. This method loads the page from :
//      - read-only sections (text,rodata) $\Rightarrow$ executive
//        file
//      - read/write sections (data,...) $\Rightarrow$ executive
//        file (1st time only), or swap file
//      - anonymous mappings (stack/bss) $\Rightarrow$ new
//        page from the MemoryManager (1st time only), or swap file
//
//	\param virtualPage the virtual page subject to the page fault
//	  (supposed to be between 0 and the
//        size of the address space, and supposed to correspond to a
//        page mapped to something [code/data/bss/...])
//	\return the exception (generally the NO_EXCEPTION constant)
*/
#ifndef ETUDIANTS_TP
ExceptionType PageFaultManager::PageFault(uint64_t virtualPage) {
  printf("**** Warning: page fault manager is not implemented yet\n");

  exit(ERROR);

  return ((ExceptionType)0);
}
#endif

#ifdef ETUDIANTS_TP

ExceptionType PageFaultManager::PageFault(uint64_t virtualPage) {
  auto old_state = g_machine->interrupt->SetStatus(IntStatus::INTERRUPTS_OFF);

  Process* p = g_current_thread->GetProcessOwner();
  TranslationTable* translationTable = p->addrspace->translationTable;


  while (translationTable->getBitIo(virtualPage))
    g_current_thread->Yield();
  
  if (translationTable->getBitValid(virtualPage)) {
    g_machine->interrupt->SetStatus(old_state);
    return NO_EXCEPTION;
  }

  translationTable->setBitIo(virtualPage);
  g_machine->interrupt->SetStatus(old_state);

  

  // Get a page in physical memory, halt of there is not sufficient space
  int pp = g_physical_mem_manager->AddPhysicalToVirtualMapping(p->addrspace, virtualPage);

  int8_t* mainMemory_pp = &(g_machine->mainMemory[pp *
                                    g_cfg->PageSize]);

  int addrDisk = translationTable->getAddrDisk(virtualPage);
  if (addrDisk == INVALID_SECTOR)
    // Fill the page with zeroes
    memset(mainMemory_pp, 0, g_cfg->PageSize);
  else {
    if (translationTable->getBitSwap(virtualPage)) {
      g_swap_manager->GetPageSwap(addrDisk, (char *) mainMemory_pp);
    } else {
      p->exec_file->ReadAt((char *) mainMemory_pp,
        g_cfg->PageSize, addrDisk);
    }
  }

  // The entry is valid
  translationTable->setBitValid(virtualPage);
  translationTable->clearBitIo(virtualPage);

  g_physical_mem_manager->UnlockPage(pp);
  return NO_EXCEPTION;
}
#endif