# SEA\_Projet\_Nachos

## Quelques détails d'implémentation

### Outils de synchronisation

Les Locks, Sémaphores et variables de conditions sont implémentées de manière très classique. On a pris soin à renvoyer la valeur `-1` et a écrire une erreur dans le cas ou l'appel systme se fait sur un argument incorrect.

### Gestion des threads

Les threads ont été implémentés de manière classique, avec la gestion de switchs de contexte, on à pris soin à sauvegarder les registres des flotants. On a aussi réstauré la TPV lors d'un changement de processus (danc la fonction RestoreProcessorState).

### Pagination à la demande

Initialement, toutes nos pages ne sont pas initialisé et entrainent donc un défaut de page. On gère le traitement de défaut de page en multi-thread, en implémentant le bit I/O qui est activé par une page qui se charge, ou par la sauvegarde d'une page en SWAP.
On initialise la variable `i_clock` à 0 (parce que `-1` faisait peu de sens...).
Le système NachOS tourne correctement avec 2 pages (même s'il est évidemment plus lent).
La tentative d'accès à une page d'un thread lorsque celle-ci est en train d'être swappé par un autre (erreur classique), ne pose pas problème avec notre implémentation.

Dans le cas pathologique d'une seule page, le système se bloque : En effet, lorsqu'un thread arrive à charger sa page, il est en attente du disque, et lorsque l'interruption arrive, le thread concurrent garde la main, et va tenter à son tour de charger sa page. Étant en attente du disque, lorsque l'interruption arrive, le thread concurrent garde la main, et va donc à son tour charger sa page, et ainsi de suite, sans jamais aboutir. (Il nous semble)

### Gestion entrée / sortie

Bien que au final pas demandé, la version bloquante des entrée sortie a été écrite pour le driver ACIA. Aucun test n'a été effectué parce que compliqué à mettre en place pour pas grand chose (mais à priori, le code devrait fonctionner...).

### Fichiers mappés

Pas fait par manque de temps 😭


## Tests

3 programmes de test ont été créée, spécifiquement `try_lock`, `try_sema` et `try_condition`, qui testent respectivement les véroux, les sémaphores, et les variables de conditions.
Ces programmes ont été éxécutés sur un nachos avec peu de pages (jusqu'à 1 seule page, on peut difficilement faire moins), en étant lancé de nombreuses fois à la suite (ce qui a permi de trouver des défauts bugs, par exemple sur une ancienne version de notre code nachos, le programme `try_condition` fonctionnait très bien la première fois, puis fonctionnait la deuxième fois sant print le premier `5`, puis fonctionnait très bien les 4 fois suivantes, avant de faire crash nachos).

Ils permettent également de tester la bonne implémentation des appels systèmes.

Pour le limit-test, on résiste très bien aux commandes `yes shell | nachos`, `yes try_sema | nachos` etc, sans tout faire crasher.


## Remarques

On a adoré travailler sur le projet NachOS, on regrette même l'absence d'une semaine de projet dédié à ça (comme en L3 par exemple).
Même si la basecode est formatté de manière... aléatoire, la présence de pleins d'outils de debug est vraiment agréable. Parfois on s'arrache les cheveux, mais c'est un bon sujet.

Merci 💚
