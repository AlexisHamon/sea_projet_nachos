#include "userlib/syscall.h"
#include "userlib/libnachos.h"


SemId s_filled;
SemId s_full;

int NUMBER_TO_PRINT = 50;
int BUFFER_SIZE = 5;
int buffer[5];

void
producteur()
{
  int buff = 0;
  for (int k = 0; k < NUMBER_TO_PRINT; k++) {
    P(s_full);
    buffer[buff] = k;
    V(s_filled);
    buff = (buff + 1) % BUFFER_SIZE;
  }

}

void
consomateur()
{
  int buff = 0;
  for (int k = 0; k < NUMBER_TO_PRINT; k++) {
    P(s_filled);
    n_printf((char*)"%d\n", buffer[buff]);
    V(s_full);
    buff = (buff + 1) % BUFFER_SIZE;
  }
}

int main()
{
  s_filled = SemCreate("s_filled", 0);
  s_full = SemCreate("s_full", BUFFER_SIZE);
  ThreadId thread_producteur = threadCreate((char*)"thread producteur", (VoidNoArgFunctionPtr) &producteur);
  ThreadId thread_consommateur = threadCreate((char*)"thread consommateur", (VoidNoArgFunctionPtr) &consomateur);
  Join(thread_producteur);
  Join(thread_consommateur);

  Exit(0);
}
