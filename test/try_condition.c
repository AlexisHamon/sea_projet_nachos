#include "userlib/syscall.h"
#include "userlib/libnachos.h"


CondId wall;

void
counter_thread()
{
  for (int i = 5; i >= 0; i--) {
    n_printf("%d\n", i);
  }
  CondBroadcast(wall);
}

void
waiter_thread()
{
  CondWait(wall);
  n_printf("Libéré !\n");
}

int
main()
{
  wall = CondCreate("wall");
  ThreadId thread_counter = threadCreate((char*)"thread counter", (VoidNoArgFunctionPtr) &counter_thread);
  ThreadId threads_waiters[5];

  for (int i = 0; i < 5; i++) {
    threads_waiters[i] = threadCreate((char*)"thread waiter", (VoidNoArgFunctionPtr)&waiter_thread);
  }

  Join(thread_counter);

  for (int i = 0; i < 5; i++) {
    Join(threads_waiters[i]);
  }

  Exit(0);
}
