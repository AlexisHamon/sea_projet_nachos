/* try_sema.c
 * 
 * Simple programm to try the implementation of semaphores.
 *
 */

// Nachos system calls
#include "userlib/syscall.h"
#include "userlib/libnachos.h"


int counter = 0;
LockId counter_lock;

void
thread_function()
{
    n_printf((char*)"Inside thread_function\n");
    for (int i = 0; i < 1000; i++) {
        LockAcquire(counter_lock);
        counter ++;
        LockRelease(counter_lock);
    }
}

int
main()
{
    counter_lock = LockCreate((char*)"counter lock");
    ThreadId first_thread = threadCreate((char*)"first_thread", (VoidNoArgFunctionPtr) &thread_function);
    ThreadId second_thread = threadCreate((char*)"second_thread", (VoidNoArgFunctionPtr) &thread_function);
    Join(first_thread);
    Join(second_thread);
    n_printf((char*)"Résultat : %d\n", counter);
    Exit(0);
}
